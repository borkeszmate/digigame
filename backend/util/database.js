// const Sequelize = require('sequelize');

// const sequelize = new Sequelize('digigame-db-dev', 'root', 'root', {
// 	host: 'localhost',
// 	dialect: 'mysql'
// });

// module.exports = sequelize;



const mysql = require('mysql2');

const pool = mysql.createPool({
	host: 'localhost',
	user: 'root',
	password: 'root',
	database: 'digigame-db-dev',
	waitForConnections: true,
	connectionLimit: 10,
	queueLimit: 0
});

// const pool = mysql.createPool({
// 	host: 'localhost',
// 	user: 'digiminer',
// 	password: '9Vzzy24QtWa4!',
// 	database: 'digigame-db-dev',
// 	waitForConnections: true,
// 	connectionLimit: 10,
// 	queueLimit: 0
// });

module.exports = pool;
