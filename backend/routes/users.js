var express = require('express');
var router = express.Router();
const UserController = require('../controllers/User');



router.post('/save-user', UserController.saveUser);
router.post('/add-existing-user', UserController.addExistingUser);

router.get('/get-users', UserController.getUsers);

router.get('/clear-users', UserController.clearUsers);

router.post('/game-finish', UserController.gameFinish);

router.get('/get-results', UserController.getResults);

module.exports = router;
