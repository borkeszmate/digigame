const game = require('../controllers/game');


exports.emitUser = () => {
	serverEmitter.emit('user', game.getUsers());
}

module.exports = (io, res) => {
	io.on('connection', (socket) => {
		console.log('connection');
		socket.on('disconnect', () => {
			console.log('An user disconnected');
		});

		socket.on('new-data', (data) => {
			game.updateUser(data);
			socket.broadcast.emit('new-data', data);
		});

		socket.on('update-user', (user) => {
			const updatedUsers = game.updateUser(user);
			socket.emit('user-update', updatedUsers);
			socket.broadcast.emit('user-update', updatedUsers);
		});

		socket.on('clear-single-user', (user) => {
			game.cleanSingleUser(user);
			socket.broadcast.emit('clear-single-user', user);
		});

		socket.on('start-game', (user) => {
			socket.broadcast.emit('start-game');
		});

		serverEmitter.on('user', function (data) {
			socket.emit('user', data);
		});

		serverEmitter.on('clear-users', function (data) {
			socket.emit('clear-users', data);
		});
	});


}



