const game = require('../controllers/game');
const pool = require('../util/database');


exports.saveUser = (req, res, next) => {
	let responseData = {
		type: 'error',
		user: req.body
	};

	const user = {
		name : req.body.userName.toUpperCase(),
		ready: req.body.ready,
		points: req.body.points,
		gameType: req.body.gameType,
		avatar: req.body.avatar
	}
	console.log(req.body);



	pool.query('SELECT * FROM `users` WHERE `user_name` = ?', [user.name], (err, result) => {

		if (result.length) {
			console.log('már létezik');
			responseData.user = user;
			responseData.message = 'Már létezik a felhasználó',
			res.status(409);
			res.json(responseData);

		} else {
			pool.query('INSERT INTO `users` (user_name, avatar) VALUES (?, ?)', [user.name, user.avatar], (err, result) => {
				if (err === null) {
					user.id = result.insertId;
					if (user.gameType === 'multi') {
						game.saveUser(user);
						serverEmitter.emit('user', game.getUsers());
					}
					responseData.type = 'success';
					responseData.user = user;

					res.json(responseData);
				} else {
					console.log(err);
				}
			});
		}
	});



}

exports.addExistingUser = (req, res, next) => {

	let responseData = {
		type: 'error',
		user: req.body
	};
	const user = {
		name : req.body.name,
		ready: req.body.ready,
		points: req.body.points,
		gameType: req.body.gameType,
		avatar: req.body.avatar
	}
	game.saveUser(user);
	serverEmitter.emit('user', game.getUsers());

	responseData.type = 'success';
	responseData.user = user;

	res.json(responseData);
}


exports.getUsers = (req, res, next) => {
	const users = game.getUsers();
	res.json(users);
}

exports.clearUsers = (req, res, next) => {
	const users = game.clearUsers();

	let responseData = {
		type: 'error'
	};

	if (!users.length) {
		responseData.type = 'success';
		responseData.message = 'Users succesfully cleared',
		responseData.users = [];

		serverEmitter.emit('clear-users', {
			clearUsers: 'Success',
			message: 'Users has been successfully cleared!'
		});
	}
	res.json(responseData);
}


exports.gameFinish = (req, res, next) => {
	console.log(req.body);

	const user = {
		id: req.body.id,
		name : req.body.name,
		points: req.body.points,
		gameType: req.body.gameType,
	};

	pool.query('INSERT INTO `results` (user_name, division, points, user_id) VALUES (?, ?, ?, ?)', [user.name, 'divízió', user.points, user.id], (err, result) => {
		if (err === null) {
			const users = game.getUsers();
			res.json({
				status: 'success',
				users: users
			});
		} else {
			console.log(err);
			res.status(500);
			res.json({
				status: 'error'
			});
		}
	});
}

exports.getResults = (req, res, next) => {
	pool.query('SELECT * FROM `results`', (err, results) => {
		if(!err) {
			console.log(results);
			res.json({
				status: 'success',
				result: results
			});

		} else {
			console.log(err);
			res.status(500);
			res.json({
				status: 'error'
			});
		}
	})
}
