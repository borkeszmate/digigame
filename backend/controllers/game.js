
module.exports = {
	users: [],

	saveUser: function(user) {
		this.users = [...this.users, user];
		console.log(user);
		return true;
	},

	getUsers: function() {
		return this.users;
	},

	updateUser: function (updatedUser) {
		this.users.forEach((user, index) => {
			if (user.id === updatedUser.id) {
				this.users[index] = updatedUser;
			}
			console.log(this.users);
		});
		return {
			users: this.getUsers(),
			updatedUser: updatedUser
		}
	},

	clearUsers: function() {
		this.users = [];
		console.log(this.users);
		return this.users;
	},

	cleanSingleUser: function(userToDelete) {
		this.users.forEach((user, index) => {
			if (user.id === userToDelete.id) {
				this.users.splice(index, 1);
			}
		})
	}

}

