import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ChannelsComponent } from './components/channels/channels.component';
import { AdminComponent } from './components/admin/admin.component';
import { LeaderboardComponent } from './components/leaderboard/leaderboard.component';
import { MonitorComponent } from './components/monitor/monitor.component';

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'canvas', component: ChannelsComponent },
	{ path: 'admin', component: AdminComponent },
	{ path: 'leaderboard', component: LeaderboardComponent },
	{ path: 'monitor', component: MonitorComponent },

];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
