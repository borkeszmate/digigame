import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SocketService } from 'src/app/services/socket.service';
import { GeneralService } from 'src/app/services/general.service';

import { Observable } from 'rxjs';
import { User } from '../../models/user';

@Component({
	selector: 'app-monitor',
	templateUrl: './monitor.component.html',
	styleUrls: ['./monitor.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class MonitorComponent implements OnInit {

	constructor(private socket: SocketService, private generalService: GeneralService) { }

	users = [];
	countdownInterval;

	starterTime: number = 360;
	remainingTime: number = 360;
	minsRemaining: number;
	secsRemaining: number;
	playing: boolean = false;
	isStarted: boolean = false;

	results;


	audios = [
		'/assets/audio/augmented_reality.mp3',
		'/assets/audio/agglomeracio.mp3',
		'/assets/audio/agilis_vezeto.mp3',
		'/assets/audio/AR-VR.mp3',
		'/assets/audio/VR.mp3',
		'/assets/audio/azurkinekt.mp3',
		'/assets/audio/bazzeg_bufe.mp3',
		'/assets/audio/szabi_kave.mp3',
		'/assets/audio/cashtime.mp3',
		'/assets/audio/az_uj_modszertan.mp3',
		'/assets/audio/digitalis_infastruktura.mp3',
		'/assets/audio/digitalizacio.mp3',
		'/assets/audio/halalkoktel.mp3',
		'/assets/audio/kinek_a_dolga.mp3',
		'/assets/audio/mesterseges_inteligencia.mp3',
		'/assets/audio/quiz.mp3',
		'/assets/audio/spinoff.mp3',
		'/assets/audio/szabi_kólái.mp3',
	];

	ngOnInit() {

		// this.playAudio(0);
		this.generalService.gameStart.subscribe(res => {
			console.log(res);
			if (!this.isStarted && res.isStarted) {
				this.isStarted = true;
				this.countdown();
			}
		});

		this.generalService.getUsers().subscribe((users: Observable<User>[]) => {
			this.users = users;
		});

		this.socket.getUsers().subscribe(res => {
			// console.log(res)
			this.users = [...this.users, ...res[res.length -1]];
			// this.setCurrentUserId();
		});

		this.socket.updateUsers().subscribe(res => {
			this.users.forEach((user, index) => {
				if (user.id === res.updatedUser.id) {
					this.users[index] = res.updatedUser;
				};
			});
		});


		this.socket.onGameStart().subscribe(() => {
			console.log('gamestart')
			this.playing = true;
			// this.countdown();
			// this.currentAppState = 'briefing';
			// this.briefing();

		});

		this.socket.clearUsers().subscribe(res => {
			if (res.clearUsers === 'Success') {
				this.users = [];
			}
		});

		this.socket.onclearSingle().subscribe(res => {
			this.users.forEach((user, index) => {
				if (user.id === res.id) {
					this.users.splice(index, 1);
				}
			})
			// console.log(this.users);
		});

		this.socket.getData().subscribe(response => {

			this.users.forEach((user, index) => {
				if (response.id === user.id) {
					this.users[index] = response;
				}
			});
		this.users = this.users.sort((a, b) => {
				return b.points - a.points;
			})
			// console.log(this.users);
		});

		this.generalService.getResults().subscribe((res) => {
			// console.log(res);
			this.results = res['result'].sort((a,b) => {
				return b.points - a.points;
			});

			// console.log(this.results);
		})
	}


	playAudio(index) {
		const audio = new Audio(this.audios[index]);
		audio.play();
		setTimeout(() => {
			this.playAudio(index+=1);
		}, 15000);
	}

	countdown() {
		this.countdownInterval = setInterval(() => {

			this.remainingTime -= 1;

			this.getRemaingTime();
			console.log(this.remainingTime);

			if (this.remainingTime === 0) {
				this.isStarted = false;
			}
		}, 1000);
	}

	getRemaingTime() {
		this.minsRemaining = Math.floor(this.remainingTime / 60);
		this.secsRemaining = this.remainingTime - (this.minsRemaining * 60)
		// console.log(this.minsRemaining, this.secsRemaining);
	}
}
