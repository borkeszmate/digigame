import { Component, OnInit } from '@angular/core';
import { SocketService } from '../../services/socket.service';
import { GeneralService } from 'src/app/services/general.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-leaderboard',
	templateUrl: './leaderboard.component.html',
	styleUrls: ['./leaderboard.component.scss']
})

export class LeaderboardComponent implements OnInit {

	currentUser;
	results;

	constructor(
		private socket: SocketService,
		private generalService: GeneralService,
		private router: Router
	) { }

	ngOnInit() {
		console.log(this.currentUser);
		this.generalService.currentUser.subscribe(user => {
			console.log(user);
			this.currentUser = user;
		});

		this.socket.clearUsers().subscribe(res => {
			if (res.clearUsers === 'Success') {
				this.router.navigate(['']);
			}
		});

		// this.generalService.getUsers().subscribe(users => {
		// 	this.users = users;
		// 	console.log(this.users);
		// });

		this.generalService.getResults().subscribe((res) => {
			console.log(res);
			this.results = res['result'].sort((a,b) => {
				return b.points - a.points;
			});

			console.log(this.results);
		})
	}

	resetUser() {
		this.router.navigate(['']);
		if (this.currentUser.user !== undefined) {
			this.socket.clearSingleUser(this.currentUser);
		}
	}

}
