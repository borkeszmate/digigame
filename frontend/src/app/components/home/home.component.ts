import { Component, OnInit, Renderer2, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { SocketService } from '../../services/socket.service';
import { GeneralService } from 'src/app/services/general.service';
import { Router } from '@angular/router';
import { GameplayService } from 'src/app/services/gameplay.service';

import { Observable } from 'rxjs';
import { User } from '../../models/user';

import { SwiperConfigInterface } from 'ngx-swiper-wrapper';





@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

	public config: SwiperConfigInterface = {
		a11y: true,
		direction: 'horizontal',
		slidesPerView: 6,
		keyboard: true,
		mousewheel: true,
		scrollbar: false,
		navigation: false,
		pagination: false,
		centeredSlides: true,
		loop: true,
		spaceBetween: 5,
	};

	dummyUsers = [
		{ name: 'Player Name 1', points: 100002},
		{ name: 'Player Name 2', points: 100001},
		{ name: 'Player Name 3', points: 100000},
	];

	results = [];



	@ViewChild('bonusesContainer') bonusesContainer: ElementRef;
	@ViewChild('multiModalContainer') multiModalContainer: ElementRef;
	@ViewChild('bonusesModalElement') bonuses: ElementRef;
	@ViewChild('multiModalElement') multiModalElement: ElementRef;
	@ViewChild('generalModal') generalModalContainer: ElementRef;
	@ViewChild('generalModalElement') generalModalElement: ElementRef;
	@ViewChild('playarea') playarea: ElementRef;
	@ViewChild('moneybox') moneybox: ElementRef;
	// Sets game type (single || multi)
	gameType: string;

	// Brief the user
	briefTextStarter: string = '';
	breifTextFinished: string = '';
	userExists = {
		ifExist: false,
		user: undefined
	};

	// Set which game components should be displayed
	currentAppState: string;

	// For styling porpuses mainly
	userName: string;

	// Before game countback
	countBackNum: number = 5;

	// User specific
	users = [];
	currentUser;
	partners: { userName: string, ready: boolean, points: number } [] = [];

	// Gameplay
	starterTime: number = 360;
	remainingTime: number = 360;
	DCKatt: number = 1;
	DPS: number = 0;
	usedInvestments = {};
	investments = this.gamePlayService.investments;
	activatedInvestments = [];
	minsRemaining: number;
	secsRemaining: number;


	// Modal

	bonusModal = {
		title: undefined,
		description: undefined,
		button: {
			type: undefined,
			path: undefined,
			color: undefined,
			text: undefined,
			disabled: true
		}
	}

	multiModal = {
		id: undefined,
		title: undefined,
		description: undefined,
		button: {
			type: undefined,
			path: undefined,
			text: undefined,
			options: undefined,
			color: undefined,
			disabled: true
		},
		userChoice: undefined,
		effect: undefined,
		choiceResult: undefined
	}

	generalModalData = {
		show: false,
		id: undefined,
		title: undefined,
		description: undefined,
		button: {
			type: undefined,
			path: undefined,
			text: undefined,
			options: undefined,
			color: undefined,
			disabled: true
		},
		onChoiceClick: undefined,
		effect: undefined,
		choiceResult: undefined
	}

	// Ingame intervals
	interval;
	countdownInterval
	gameInterval;

	// Sets state
	isFinished: boolean = false;
	avatarIndex: number = 0;


	bonusArr = [
			{
			name: 'Csengődi csütörtök',
			description: 'Feleződő Hit',
			trigger: () => {
				return (this.remainingTime === this.starterTime - 60) ? true : false;
				// return (this.remainingTime === this.starterTime - 2) ? true : false;
			},
			setModal: () => {
				this.bonusModal = {
					title: 'CSENGŐDI CSÜTÖRTÖK!',
					description: 'Túlságosan bebasztál, 1  hét másnap a sok tüskétől... Feleződik a TAP-pelős pénztermelésed 30 másodpercig:(',
					button: {
						type: 'element',
						path: 'assets/img/bonus/csengodics.png',
						color: '#ea3952',
						text: 'Dikk',
						disabled: true
					}
				}

				setTimeout(() => {
					this.bonusModal.button.disabled = false;
				}, 2000);
			},
			effect : () => {
				this.DCKatt /= 2;
				setTimeout(() => {
					this.DCKatt *=2;
					console.log('DPS visszaállítás');
				}, 30000);
			}
		},
		{
			name: 'Feat agency',
			description: 'Digital gyakornokok száma feleződik (lefelé kerekítve) és -2 Digital PM',
			trigger: () => {
				return (this.remainingTime === this.starterTime - 180) ? true : false;
			},
			setModal: () => {
				this.bonusModal = {
					title: 'FEAT AGENCY!',
					description: 'Atesz elvitte a gyakornokaid felét + a két legjobb digitál PM-ed is... :(',
					button: {
						type: 'element',
						path: 'assets/img/bonus/Feat.png',
						color: '#ea3952',
						text: 'A szemét!',
						disabled: true
					}
				}

				setTimeout(() => {
					this.bonusModal.button.disabled = false;
				}, 2000);
			},
			effect : () => {
				if (this.usedInvestments['Intern'] < 2)
				{
					this.DPS -= this.usedInvestments['Intern'] * 0.5;
					this.usedInvestments['Intern'] = 0;
				} else {
					if (this.DPS > 0) {
						this.DPS -= (Math.floor(this.usedInvestments['Intern']) / 2) * 0.5;
						this.usedInvestments['Intern'] = Math.floor(this.usedInvestments['Intern'] / 2);
					}
				}

				if (this.usedInvestments['Project Manager'] - 2 > 0) {
					this.usedInvestments['Project Manager'] = this.usedInvestments['Project Manager'] - 2;
					this.DCKatt -= this.usedInvestments['Project Manager'];
					this.DPS-= this.usedInvestments['Project Manager'] * 3;
				} else {
					if (this.DPS > 0 && this.DCKatt > 0) {
						this.usedInvestments['Project Manager'] = 0;
						this.DCKatt -= this.usedInvestments['Project Manager'];
						this.DPS-= this.usedInvestments['Project Manager'] * 3;
					}
				}

			}
		},
		{
			name: 'Budget cut',
			description: '-500000 DC',
			trigger: () => {
				return (this.remainingTime === this.starterTime - 330) ? true : false;
			},
			setModal: () => {
				this.bonusModal = {
					title: 'BUDGET CUT!!',
					description: 'Az ügyfeled anyacége elzárta a pénzforrásokat, kevesebb szolgáltatást tud venni tőled... -500.000DC',
					button: {
						type: 'element',
						path: 'assets/img/bonus/BudgetCut.png',
						color: '#ea3952',
						text: 'Basszus!',
						disabled: true
					}
				}
				setTimeout(() => {
					this.bonusModal.button.disabled = false;
				}, 2000);
			},
			effect : () => {
				if (this.currentUser.points > 500000) {
					this.currentUser.points -= 500000;

				} else {
					this.currentUser.points = 0;
				}
			}
		},
		{
			name: 'Sport szelet',
			description: 'Háromszorozódik a hit 15 sec-ig',
			trigger: () => {
				return (this.remainingTime === this.starterTime - 30) ? true : false;
			},
			setModal: () => {
				this.bonusModal = {
					title: 'SPORT SZELET!',
					description: 'A kultikus csoki szupererőt ad... Háromszorosan termeled mostantól a pénzt TAP-pel',
					button: {
						type: 'element',
						path: 'assets/img/bonus/Sport.png',
						color: '#009e5c',
						text: 'Hurrá!',
						disabled: true
					}
				}
				setTimeout(() => {
					this.bonusModal.button.disabled = false;
				}, 2000);
			},
			effect : () => {
				this.DCKatt *= 3;
				setTimeout(() => {
					this.DCKatt /=3;
				}, 15000);
			}
		},
		{
			name: 'Csengődi hétvége',
			description: 'Kétszeresen termel a DCpS 60 sec-ig',
			trigger: () => {
				return (this.remainingTime === this.starterTime - 300) ? true : false;
			},
			setModal: () => {
				this.bonusModal = {
					title: 'CSENGŐDI HÉTVÉGE!',
					description: 'Túlestél a HOLDponton, és örökre bebaszva maradtál... Végtelen számú jó ötleted van, mostantól a játék végéig duplán termel a csapat!',
					button: {
						type: 'element',
						path: 'assets/img/bonus/csengodih.png',
						color: '#009e5c',
						text: 'Ezaz dolgozzanak csak keményen!',
						disabled: true

					}
				}
				setTimeout(() => {
					this.bonusModal.button.disabled = false;
				}, 2000);
			},
			effect : () => {
				this.DPS *= 2;
				setTimeout(() => {
					this.DPS /=2;
				}, 60000);
			}
		},
		{
			name: 'Ügyfélajándék',
			description: 'Ötszöröződik a hit 30 sec-ig',
			trigger: () => {
				return (this.remainingTime === this.starterTime - 210) ? true : false;
			},
			setModal: () => {
				this.bonusModal = {
					title: 'AJÁNDÉK!',
					description: 'Egy időben kaptál az ügyfeleidtől csokit, napszemüveget és Sziget belépőt... Mostantól 5-szörösen termeled a DC-t a TAP-pel!',
					button: {
						type: 'element',
						path: 'assets/img/bonus/ajandek.png',
						color: '#009e5c',
						text: 'Éljen!',
						disabled: true
					}
				}

				setTimeout(() => {
					this.bonusModal.button.disabled = false;
				}, 2000);
			},
			effect : () => {
				this.DCKatt *= 5;
				setTimeout(() => {
					this.DCKatt /= 5;
				}, 15000);
			}
		},
	]


	multiModalArrSeeder = [
		{
			name: 'Random ügyfél komment',
			description: ' +20000DC most VAGY 60sec múlva +50000DC',
			isUsed: false,
			trigger: function (usedInvestments, remainingTime, starterTime ) {
				// if (Object.keys(usedInvestments).includes('Social Media Team') && Object.keys(usedInvestments).includes('Copywriter') &&!this.isUsed) {
				// 	this.isUsed = true;
				// 	return true;
				// } else {
				// 	return false;
				// }

				return (remainingTime === starterTime - 240) ? true : false;

			},
			setModal: function (modal) {
				modal = {
					id: 1,
					title: 'Kérlek módosítsd a facebookos poszt kreatívját!',
					description: 'Ügyfelednek mégsem tetszenek a feliratok a már posztolt kreatívokon. Módosítod őket?',
					button: {
						type: 'multiChoice',
						path: 'assets/img/bonus/FB_kreativ.png',
						text: 'Hurrá!',
						options: ['IGEN', 'NEM'],
						color: '#6b3695',
						disabled: true
					},
				},

				this.showModal(modal);
			},

			showModal: (modal) => {
				this.multiModal = modal;
				setTimeout(() => {
					this.multiModal.button.disabled = false;
				}, 2000);
			},

			effect : (modal, result) => {

				this.multiModal = modal;
				if (result) {
					this.multiModal.description = 'Ez bejött... (+20000DC)';
					this.multiModal.button.type = 'singleChoice',
					this.multiModal.button.color = '#009e5c'
					this.multiModal.button.text = 'Ezt már szeretem!'
					this.currentUser.points += 20000;
					setTimeout(() => {
						this.dismissMultiModal();
					}, 5000);
				} else {
					this.multiModal.description = 'Bevált az érvelésed, mivel egyeztettél a Social Media Teammel, a kampány kurvajól teljesített. +50000DC!';
					this.multiModal.button.type = 'singleChoice',
					setTimeout(() => {
						this.currentUser.points += 50000;
					}, 15000);

					setTimeout(() => {
						this.dismissMultiModal();
					}, 5000);
				}
			}
		},


		{
			name: 'Freelancer UX designer',
			description: '0 VAGY -20000DC +100 DCpS játék végéig',
			isUsed: false,
			trigger: function (usedInvestments, currentUser, remainingTime, starterTime) {
				// if (Object.keys(usedInvestments).includes('Social Media Team') && currentUser.points >= 3000 && !this.isUsed) {
				// 	this.isUsed = true;
				// 	return true;
				// } else {
				// 	return false;
				// }

				return (remainingTime === starterTime - 240) ? true : false;

			},
			setModal: function (modal) {
				modal = {
					id: 2,
					title: 'UX Designer a projektre?',
					description: 'Indul a webfejlesztés és szűkös a budget. Bevonsz egy költséges UX designert a projektbe?',
					button: {
						type: 'multiChoice',
						path: 'assets/img/bonus/UX.png',
						text: 'Hurrá!',
						options: ['LEGYEN (-20000 DC)', 'SPÓROLJUNK'],
						color: '#6b3695',
						disabled: true
					},
				},

				this.showModal(modal);
			},

			showModal: (modal) => {
				this.multiModal = modal;
				setTimeout(() => {
					this.multiModal.button.disabled = false;
				}, 2000);
			},

			effect : (modal, result) => {

				this.multiModal = modal;
				if (result) {

					this.multiModal.description = 'Az UX-es nagyon rápörgött a projektre, sok minden kiderült a készülő website-ról. Optimalizálta a projektet, ez másodpercenként +100DC-t jelent neked a játék végéig!';
					this.multiModal.button.type = 'singleChoice';
					this.multiModal.button.color = '#6b3695';
					this.multiModal.button.text = 'Hurrá, ez is bejött!'
					this.DPS += 300;
					this.currentUser.points -= 20000;
					setTimeout(() => {
						this.dismissMultiModal();
					}, 5000);
				} else {

					this.multiModal.description = 'Huhh, ez most nem került semmibe...';
					this.multiModal.button.type = 'singleChoice',
					this.multiModal.button.color = '#6b3695';
					this.multiModal.button.text = 'Hurrá, ez is bejött!'
					setTimeout(() => {
						this.dismissMultiModal();
					}, 5000);
				}
			}
		},

		{
			name: 'Digital Media Tervezés',
			description: '0 VAGY -2000DC és 90sec múlva +20000DC',
			isUsed: false,
			trigger: function (usedInvestments, currentUser, remainingTime, starterTime) {
				// if (Object.keys(usedInvestments).includes('Performance Team') && currentUser.points >= 2000 && !this.isUsed) {
				// 	this.isUsed = true;
				// 	return true;
				// } else {
				// 	return false;
				// }
				return (remainingTime === starterTime - 240) ? true : false;

			},
			setModal: function (modal) {
				modal = {
					id: 3,
					title: 'Integrált kampány digital médiával!',
					description: 'Elindult az ötletelés... Behívod a performance szakértőket a kickoff meetingre?',
					button: {
						type: 'multiChoice',
						path: 'assets/img/gamer.png',
						text: 'Hurrá!',
						options: ['JÖJJENEK (-20000 DC)', 'SPÓROLJUNK'],
						disabled: true
					},
				},

				this.showModal(modal);
			},

			showModal: (modal) => {
				this.multiModal = modal;
				setTimeout(() => {
					this.multiModal.button.disabled = false;
				}, 2000);
			},

			effect : (modal, result) => {

				this.multiModal = modal;
				if (result) {

					this.multiModal.description = 'Az ötletelésen rávilágítottak a digitál médiás kollégák, hogy fontos lenne a videó produkciót a digitál kampány célokhoz igazítani... Rengeteg feliratkozás jött! +60000DC!';
					this.multiModal.button.type = 'singleChoice',
					this.currentUser.points -= 20000;
					setTimeout(() => {
						this.dismissMultiModal();
					}, 5000);
					setTimeout(() => {
						this.currentUser.points += 60000;
					}, 15000);
				} else {
					this.multiModal.description = 'Huhh, ez most nem került semmibe...';
					this.multiModal.button.type = 'singleChoice',
					this.multiModal.button.color = '#6b3695';
					this.multiModal.button.text = 'Oké!';
					setTimeout(() => {
						this.dismissMultiModal();
					}, 5000);
				}
			}
		},

		{
			name: 'Customer Service Management',
			description: ' +5000 VAGY -100000 és +1505DCpS 60sec-ig',
			isUsed: false,
			trigger: function (usedInvestments, currentUser, remainingTime, starterTime) {
				// if (Object.keys(usedInvestments).includes('Performance Team') && currentUser.points >= 2000 && !this.isUsed) {
				// 	this.isUsed = true;
				// 	return true;
				// } else {
				// 	return false;
				// }

				return (remainingTime === starterTime - 240) ? true : false;

			},
			setModal: function (modal) {
				modal = {
					id: 4,
					title: 'Nyereményjáték!',
					description: 'Váratlan helyzet, le kell vezényelni egy online nyereményjátékot... Customer Service kollégát kéred meg erre vagy inkább elintézed te magad?',
					button: {
						type: 'multiChoice',
						path: 'assets/img/gamer.png',
						text: 'Hurrá!',
						options: ['INTÉZEM ÉN', 'KELL A SEGÍTSÉG (-10000 DC)'],
						disabled: true
					},
				},

				this.showModal(modal);
			},

			showModal: (modal) => {
				this.multiModal = modal;
				setTimeout(() => {
					this.multiModal.button.disabled = false;
				}, 2000);
			},

			effect : (modal, result) => {

				this.multiModal = modal;
				if (result) {

					this.multiModal.description = 'Chillbe lement a játék, és egészen jól összehoztad te magad is... +5000DC!';
					this.multiModal.button.type = 'singleChoice',
					this.multiModal.button.text = 'Szupi!'
					this.multiModal.button.color = '#6b3695';
					this.currentUser.points += 5000;
					setTimeout(() => {
						this.dismissMultiModal();
					}, 5000);
				} else {
					this.multiModal.description = 'Jó döntés! A Customer Service-es csapat kifaszázta a nyereményjátékot, jutalmad +300DC másodpercenként egészen 1 percig!';
					this.multiModal.button.type = 'singleChoice',
					this.multiModal.button.text = 'Királyság!'
					this.multiModal.button.color = '#6b3695';
					this.currentUser.points -= 10000;
					this.DPS += 300;
					setTimeout(() => {
						this.dismissMultiModal();
					}, 5000);
					setTimeout(() => {
						this.DPS -= 300;
					}, 60000);
				}
			}
		},

		// {
		// 	name: 'Margin Mission',
		// 	description: '+5000DC VAGY @5:00 siker esetén=@50000DC +50000DC',
		// 	isUsed: false,
		// 	trigger: function (usedInvestments, currentUser) {
		// 		if (Object.keys(usedInvestments).includes('Performance Team') && currentUser.points >= 2000 && !this.isUsed) {
		// 			this.isUsed = true;
		// 			return true;
		// 		} else {
		// 			return false;
		// 		}

		// 	},
		// 	setModal: function (modal) {
		// 		modal = {
		// 			id: 5,
		// 			title: 'MARGIN MISSION!',
		// 			description: 'Teljesíteni kell, hozd ki a legtöbbet a projektből! 5:00-ig érj el 50000DC-t egy kis jutalomért cserébe... Vállalod? Jaaa, most mész szabira?',
		// 			button: {
		// 				type: 'multiChoice',
		// 				path: 'assets/img/gamer.png',
		// 				text: 'Hurrá!',
		// 				options: ['VÁLLALLOM', 'SZABI (+5000 DC)']
		// 			},
		// 		},

		// 		this.showModal(modal);
		// 	},

		// 	showModal: (modal) => {
		// 		this.multiModal = modal;
		// 	},

		// 	effect : (modal, result) => {

		// 		this.multiModal = modal;
		// 		if (result) {

		// 			this.multiModal.description = 'Ingyen zsé!';
		// 			this.multiModal.button.type = 'singleChoice',

		// 			// itt nem tudom milyen bónusznak kell történnie.

		// 			setTimeout(() => {
		// 				this.dismissMultiModal();
		// 			}, 5000);
		// 		} else {

		// 			this.multiModal.description = 'Jó döntés! A Customer Service-es csapat kifaszázta a nyereményjátékot, jutalmad +125DC másodpercenként egészen 1 percig!';
		// 			this.multiModal.button.type = 'singleChoice',
		// 			this.DPS += 125;
		// 			setTimeout(() => {
		// 				this.dismissMultiModal();
		// 			}, 5000);
		// 			setTimeout(() => {
		// 				this.DPS -= 125;
		// 			}, 60000);
		// 		}
		// 	}
		// },


	]

	multiModalArr: any = [
		{
			name: 'Tender nyerés',
			description: 'Stratégia megvásárlása + 3sec. +7500',
			isUsed: false,
			trigger: function (usedInvestments) {
				if (Object.keys(usedInvestments).includes('Strategyst') && !this.isUsed) {
					this.isUsed = true;
					return true;
				} else {
					return false;
				}

			},
			setModal: function (modal) {
				modal = {
					id: 0,
					title: 'Gratula!',
					description: 'A stratégával való együttműködés meghozta a gyümölcsét. Megnyerted a tendert! +7500DC',
					button: {
						type: 'singleChoice',
						path: 'assets/img/bonus/Gratula.png',
						text: 'Hurrá!',
						options: [],
						color: '#009e5c',
						disabled: true
					},
					effect: undefined,
					choiceResult: undefined
				}
				this.effect(modal);
			},
			effect : (modal) => {
				this.currentUser.points += 7500;
				this.multiModal = modal;

				setTimeout(() => {
					this.multiModal.button.disabled = false;
				}, 2000);
			}
		},
	]


	constructor(
		private socket: SocketService,
		private renderer: Renderer2,
		private generalService: GeneralService,
		private router: Router,
		private gamePlayService: GameplayService
	) { }

	ngOnInit() {

		this.currentAppState = 'gameType';
		// this.countBack();
		// this.countdown();

		this.generalService.getUsers().subscribe((users: Observable<User>[]) => {
			this.users = users;
		})

		// Socket listening on changes
		this.socket.getUsers().subscribe(res => {
			this.users = [...this.users, ...res[res.length -1]];
			// this.setCurrentUserId();
			console.log(this.users);
		});

		this.socket.updateUsers().subscribe(res => {
			console.log(res);
			this.users.forEach((user, index) => {
				if (user.id === res.updatedUser.id) {
					this.users[index] = res.updatedUser;
				};
			});
		});


		this.socket.onGameStart().subscribe(() => {
			this.currentAppState = 'briefing';
			this.briefing();
		});


		this.socket.clearUsers().subscribe(res => {
			if (res.clearUsers === 'Success') {
				this.userName = undefined;
				this.currentUser = undefined;
				this.users = [];
				this.currentAppState = 'gameType';
				this.clearAllIntervals();
				location.reload();
			}
		});

		this.socket.onclearSingle().subscribe(res => {
			console.log(this.currentUser);
			if (res.id === this.currentUser.id) {
				this.userName = undefined;
				this.currentUser = undefined;
				this.users = [];
				this.currentAppState = 'gameType';
				this.clearAllIntervals();
				location.reload();
			}
			this.users.forEach((user, index) => {
				if (user.id === res.id) {
					this.users.splice(index, 1);
				}
			})
		});


		this.multiModalArr = [...this.multiModalArr, this.multiModalArrSeeder[Math.floor(Math.random()*3)] ];


		this.generalService.getResults().subscribe((res) => {

			const temp = res['result'].sort((a,b) => {
				return b.points - a.points;
			});

			for ( let i = 0; i < 3; i++ ) {
				this.results = [...this.results, temp[i]];
			}


			console.log(this.results);
		});

	}

	ngAfterViewInit() {

		// this.hideElement(0);
	}

	hideElement(time) {

		if (time === 5.5) {
			this.playarea.nativeElement.childNodes[0].childNodes[0].style.display = 'none';
			this.playarea.nativeElement.childNodes[0].childNodes[1].style.display = 'block';
		} else {
			setTimeout(() => {
				this.hideElement(time+= 0.5);
			}, 500);
		}


	}

	hit() {
		this.playarea.nativeElement.childNodes[0].childNodes[1].style.display = 'none';
		this.playarea.nativeElement.childNodes[0].childNodes[2].style.display = 'block';
		this.mine();
		const money = this.renderer.createElement('img');
		this.renderer.setAttribute(money, 'src', '/assets/img/digiminer_coin.png');
		this.renderer.addClass(money,'money');
		this.renderer.appendChild(this.moneybox.nativeElement, money);
		setTimeout(() => {
			this.renderer.removeChild(this.moneybox, money);
		}, 400);


	}

	release(time) {
		if (time === 0.1) {
			this.playarea.nativeElement.childNodes[0].childNodes[1].style.display = 'block';
			this.playarea.nativeElement.childNodes[0].childNodes[2].style.display = 'none';
		} else {
			setTimeout(() => {
				this.release(time+= 0.1);
			}, 100);
		}
	}

	isInvestmentPurchased(investment) {
		// console.log('cechinvestment');
		return (Object.keys(this.usedInvestments).includes(investment) && this.usedInvestments[investment] > 0) ? true : false
	}

	numOfInvestmentPurchased(investment) {
		// console.log('cechinvestment');
		return Object.keys(this.usedInvestments).includes(investment) ? this.usedInvestments[investment] : null
	}


	setGameType(type) {
		this.gameType = type;
		this.currentAppState = 'name';
	}

	addName() {
		this.generalService.saveUser({
			userName: this.userName,
			ready: false,
			points: 0,
			gameType: this.gameType,
			avatar: this.avatars[this.avatarIndex].url
		}).subscribe((response: {success: string, user: {name: string, ready: boolean, points: number, id: number}}) => {
			this.userName = response.user.name;
			this.currentUser = response.user;
			console.log(response);

			if (this.gameType === 'single') {
				this.currentUser = response.user;
				this.currentAppState = 'briefing';
				this.briefing();
				console.log(this.gameType);
			} else {
				this.currentAppState = 'waiting';
			}
		},
		(error) => {
			// console.log(error);
			this.userExists.ifExist = true;
			this.userExists.user = error.error.user;
			console.log(this.userExists)
			this.currentUser = error.error.user;

			this.generalModalData = {
				show: true,
				id: undefined,
				title: 'Hoppá!',
				description: 'Ezzel a felhasználónévvel már játszottak! Légyszi válasszaz alábbi lehetőségek közül!',
				button: {
					type: 'multiChoice',
					path: undefined,
					text: undefined,
					options: ['Ezzel a névvel játszok még egyet!', 'Más nevet választok'],
					color: '#6b3695',
					disabled: false
				},
				onChoiceClick: (choice) => {
					if (choice) {
						this.dismissGeneralModal();
						if (this.gameType === 'single') {
							this.currentAppState = 'briefing';
							this.briefing();
							console.log(this.gameType);
						} else {
							this.generalService.addExistingUser(this.currentUser).subscribe((res) => {
								this.currentAppState = 'waiting';
								console.log(res);
							}, (err) => {
								console.log(err);
							})
						}
					} else {
						this.dismissGeneralModal();
						this.currentUser = undefined;
						this.userExists.ifExist = false;
					}
				},
				effect: undefined,
				choiceResult: undefined
			}
			this.showGeneralModal();


		});
	}


	briefing() {
		let i = 0;
		const speed = 50;
		this.briefTextStarter = `Hello <strong> ${this.currentUser.name} </strong> <br> Te kaptad a nyakadba az egyik legvadabb ügyfelünket. Nem tudja, hogy mit akar, kinek és hogyan, de a Te felelősséged, hogy a legkirályabb projektet rakd össze neki. <br>Ahhoz, hogy a projektet beindítsd, csákányozd a DigiCoin-t Gergővel és állítsd össze a legütősebb csapatot.<strong> A feladatod, hogy a legmagasabb értéket termeld 6 perc alatt. </strong> <br> Ahogy projekted komolyodik, egyre több lehetőséged van, szakértők bevonására, ami munkád eredményére is hatással lesz. <br> Játssz megfontoltan, és készülj fel mindenre, mert rengeteg kihívást tartogat a projekt!`

		const addLetter = () => {
			if (i <= this.briefTextStarter.length + 20) {
				setTimeout(() => {
					this.breifTextFinished += this.briefTextStarter.charAt(i);
					i++;
					addLetter();
				}, speed);
				if (i === this.briefTextStarter.length + 20 ) {
					this.currentAppState = 'countback';
					this.start();
					this.countBack();
				}
			}
		}
		addLetter();
	}



	start() {
		this.DPS = this.DPS;
		this.gameInterval = setInterval(() => {
			this.currentUser.points += this.DPS;
			// console.log(this.DPS);

			if (!this.isFinished && this.gameType === 'multi') {
				this.socket.sendData(this.currentUser);
			}
		}, 1000);

		this.socket.getData().subscribe(response => {
			const current = this.partners.find(item => {
				if(item === undefined) {
					return undefined;
				}
				return item.userName === response.userName;
			});

			if (current === undefined) {
				this.partners = [...this.partners, response];
			} else {
				this.partners.forEach((item, index) => {
					if (item.userName === current.userName) {
						this.partners[index] = response
					}
				})
			}
		});
	}




	investClick(item) {
		this.DCKatt += item.DCKatt;
		this.DPS += item.DPS;
		this.currentUser.points -= item.price;
		console.log(this.usedInvestments);

		if (Object.keys(this.usedInvestments).includes(item.title)) {
			this.usedInvestments[item.title] += 1;
		} else {
			this.usedInvestments[item.title] = 1;
		}
		console.log(item);
		this.investments.forEach((investment, index) => {
			if (item.title === investment.title) {
				this.investments[index].price = this.investments[index].price + this.investments[index].price * 0.2;
				this.investments[index].price = Math.floor(this.investments[index].price);
			}
		} )

	}



	mine() {
		this.currentUser.points += this.DCKatt;

	}



	updateUser() {
		this.currentUser.ready = !this.currentUser.ready;
		this.socket.updateUser(this.currentUser);
	}



	countBack() {
		this.interval = setInterval(() => {
			this.countBackNum -= 1;
			if (this.countBackNum === 0) {
				clearInterval(this.interval);
				this.currentAppState = 'game';
				this.countdown();
			}
		}, 1000);
	}



	countdown() {
		let width :number | string = 100;
		this.hideElement(0);
		if (this.gameType === 'multi') {
			this.multiplayerGameStarted();

		}
		this.countdownInterval = setInterval(() => {
			const counter = document.querySelector('.timeline__counter');
			width = (this.remainingTime / this.starterTime) * 100;
			this.remainingTime -= 1;
			width = width.toString() + '%';
			this.renderer.setStyle(counter,'width', width);

			this.iterateBonuses();
			this.iterateMultiChoice(this.usedInvestments, this.currentUser, this.remainingTime, this.starterTime);
			this.getRemaingTime();

			if (this.remainingTime === 0) {
				this.generalService.onGameFinish(this.currentUser).subscribe(() => {
					this.generalService.transferUserData(this.currentUser);
					this.isFinished = true;
					clearInterval(this.countdownInterval);
					this.router.navigate(['/leaderboard']);
				}, (error) => {
					console.log(error);
				});
			}
		}, 1000);
	}



	setCurrentUserId() {
		for (const user of this.users) {
			if (user.userName === this.userName) {
				this.currentUser = user;
			}
		}
	}

	clearAllIntervals() {
		clearInterval(this.interval);
		clearInterval(this.gameInterval);
		clearInterval(this.countdownInterval);
	}

	showBonusModal() {
		this.bonusesContainer.nativeElement.classList.add('visible');
		this.bonuses.nativeElement.classList.add('visible');
	}

	dismissBonusModal() {

		this.bonusesContainer.nativeElement.classList.add('remove');

		this.bonuses.nativeElement.classList.add('remove');
		setTimeout(() => {
			this.bonusesContainer.nativeElement.classList.remove('visible');
			this.bonusesContainer.nativeElement.classList.remove('remove');
			this.bonuses.nativeElement.classList.remove('visible');
			this.bonuses.nativeElement.classList.remove('remove');
		}, 1000);
	}

	showMultiModal() {
		this.multiModalContainer.nativeElement.classList.add('visible');
		this.multiModalElement.nativeElement.classList.add('visible');
	}

	dismissMultiModal() {

		this.multiModalContainer.nativeElement.classList.add('remove');

		this.multiModalElement.nativeElement.classList.add('remove');
		setTimeout(() => {
			this.multiModalContainer.nativeElement.classList.remove('visible');
			this.multiModalContainer.nativeElement.classList.remove('remove');
			this.multiModalElement.nativeElement.classList.remove('visible');
			this.multiModalElement.nativeElement.classList.remove('remove');
		}, 1000);
	}

	showGeneralModal() {
		this.generalModalContainer.nativeElement.classList.add('visible');
		this.generalModalElement.nativeElement.classList.add('visible');
	}

	dismissGeneralModal() {

		this.generalModalContainer.nativeElement.classList.add('remove');

		this.generalModalElement.nativeElement.classList.add('remove');
		setTimeout(() => {
			this.generalModalContainer.nativeElement.classList.remove('visible');
			this.generalModalContainer.nativeElement.classList.remove('remove');
			this.generalModalElement.nativeElement.classList.remove('visible');
			this.generalModalElement.nativeElement.classList.remove('remove');
			this.generalModalData.show = false;
		}, 1000);
	}


	iterateBonuses() {
		for (let i = 0; i < this.bonusArr.length; i++) {
			if (this.bonusArr[i].trigger()) {
				this.bonusArr[i].setModal();
				this.bonusArr[i].effect();
				this.showBonusModal();

			}
		}
	}

	iterateMultiChoice(usedInvestments, currentUser, remainingTime, starterTime) {
		for (let i = 0; i < this.multiModalArr.length; i++) {
			if (this.multiModalArr[i].trigger(usedInvestments, currentUser, remainingTime, starterTime)) {
				this.multiModalArr[i].setModal(this.multiModal);
				// this.multiModalArr[i].effect();
				this.showMultiModal();
			}
		}
	}


	onChoiceClick(modal, choice) {
		// this.multiModalArr[modal.id].effect(modal, choice);
		this.multiModalArr[1].effect(modal, choice);
	}

	generalModalChoice(choice) {
		this.generalModalData.onChoiceClick(choice);
	}


	// Swiper
	onIndexChange(event) {
		// console.log(event);
		this.avatarIndex = event;

	}

	onSwiperEvent(event) {
		// console.log(event);
	}


	avatars = [
		{
			url: '/assets/img/icons/kid.png'
		},
		{
			url: '/assets/img/icons/doge.png'
		},
		{
			url: '/assets/img/icons/shrek.png'
		},
		{
			url: '/assets/img/icons/smart.png'
		},
		{
			url: '/assets/img/icons/crying.png'
		},
		{
			url: '/assets/img/icons/cato.png'
		},
	];

	getRemaingTime() {
		this.minsRemaining = Math.floor(this.remainingTime / 60);
		this.secsRemaining = this.remainingTime - (this.minsRemaining * 60)
		// console.log(this.minsRemaining, this.secsRemaining);
	}

	multiplayerGameStarted() {
		this.generalService.gameStarted({isStarted: true});
	}

	resetUser() {
		this.socket.clearSingleUser(this.currentUser);
		location.reload();
	}

}
