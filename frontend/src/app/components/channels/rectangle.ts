export class Rectangle {
	hx: number;
	x: number;
	vy: number;
	y: number;
	height: number;
	width: number;
	color: string;
	originalSize: number;
	maxSize: number;

	constructor(hx, x, vy, y, height, width, color) {
		this.hx = hx;
		this.x = x;
		this.vy = vy;
		this.y = y;
		this.height = height;
		this.width = width;
		this.color = color;
		this.originalSize = 20;
		this.maxSize = 100;
	}


	canvas = document.querySelector('canvas');
	ctx = this.canvas.getContext('2d');
	animate() {
		this.ctx.fillStyle = this.color;

		this.hx = this.hx + this.x;
		this.vy = this.vy + this.y;
		this.ctx.fillRect(this.hx, this.vy, this.height, this.width);

		if (this.hx > window.innerWidth && this.x > 0 ) {
			this.x = -this.x;
		}

		if (this.hx < 0) {
			this.x = -this.x;
		}

		if (this.vy > window.innerHeight && this.y > 0 ) {
			this.y = -this.y;
		}

		if (this.vy < 0) {
			this.y = -this.y;
		}

	}

	scale(mouseX: number, mouseY: number) {
		if (mouseX - this.hx < 150 && mouseX - this.hx > -150 && mouseY - this.vy < 150 && mouseY - this.vy > -150 && this.height < this.maxSize && this.width < this.maxSize ) {
			// this.color = 'red';
			this.width += 3;
			this.height += 3
		} else if (this.width > this.originalSize && this.height > this.originalSize) {
				this.width -= 3;
				this.height -= 3
		}
	}
}


