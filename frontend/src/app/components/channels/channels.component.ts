import { Component, ViewChild, ElementRef, OnInit, NgZone } from '@angular/core';
import { Rectangle } from './rectangle';



@Component({
	selector: 'app-channels',
	templateUrl: './channels.component.html',
	styleUrls: ['./channels.component.scss']
})
export class ChannelsComponent implements OnInit {

	@ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>
	ctx: CanvasRenderingContext2D;
	elements: Rectangle [] = [];
	colors = ['#05386b','#379683', '#bcdb95','#8ee4af', '#edf5e1'];

	mouse: { x: number, y: number } = {
		x: undefined,
		y: undefined
	};

	constructor(
		private ngZone: NgZone,
		) { }


	ngOnInit() {
		this.canvas.nativeElement.height = window.innerHeight;
		this.canvas.nativeElement.width = window.innerWidth;
		this.ctx = this.canvas.nativeElement.getContext('2d');

		for( let i = 0; i < 100; i++) {
			const color = this.colors[Math.floor(Math.random()* 6)]
			const rectangle = new Rectangle(Math.random() * window.innerWidth, Math.random() * 10 , Math.random() * window.innerHeight, Math.random() * 5, 20, 20, color);
			this.elements = [...this.elements,rectangle];
		}
		this.updateAll();
	}

	updateAll() {
		this.ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);
		requestAnimationFrame(() => {
			this.ngZone.runOutsideAngular(() => {
				this.updateAll();
			});
		});

		for(let rectangle of this.elements) {
			rectangle.animate();
			rectangle.scale(this.mouse.x, this.mouse.y);
		}
	}

	mouseMove(event) {
		this.mouse.x = event.x;
		this.mouse.y = event.y;
	}
}
