import { Component, OnInit } from '@angular/core';
import { SocketService } from '../../services/socket.service';
import { GeneralService } from 'src/app/services/general.service';
import { Observable } from 'rxjs';
import { User } from '../../models/user';

@Component({
	selector: 'app-admin',
	templateUrl: './admin.component.html',
	styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

	users = [];

	constructor(
		private socket: SocketService,
		private generalService: GeneralService,
	) { }

	ngOnInit() {
		this.generalService.getUsers().subscribe((users: Observable<User>[]) => {
			this.users = users;
			console.log(users);
		})

		this.socket.getUsers().subscribe(res => {
			this.users = [...this.users, ...res[res.length -1]];
		});

		this.socket.updateUsers().subscribe(res => {
			console.log(res);
			this.users.forEach((user, index) => {
				if (user.id === res.updatedUser.id) {
					this.users[index] = res.updatedUser;
				};
			});
		});

		this.socket.clearUsers().subscribe(res => {
			if (res.clearUsers === 'Success') {
				this.users = [];
			}
		});
	}


	startGame() {
		this.socket.startGame();
	}

	restartGame() {
		this.generalService.restartGame().subscribe(res => console.log(res));
	}

	refreshUsers() {
		this.generalService.getUsers().subscribe((users: Observable<User>[]) => {
			this.users = users;
		})
	}

	clearUser(user: User, i: number) {
		this.socket.clearSingleUser(user);
		this.users.splice(i, 1);
	}


}
