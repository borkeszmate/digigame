export interface User {
	name: string;
	ready: boolean;
	points: number;
	gameType: string;
	id: number;
}
