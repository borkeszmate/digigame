import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms'


import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SocketService } from './services/socket.service';
import { ChannelsComponent } from './components/channels/channels.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { HttpClientModule } from '@angular/common/http';
import { GeneralService } from './services/general.service';
import { AdminComponent } from './components/admin/admin.component';
import { LeaderboardComponent } from './components/leaderboard/leaderboard.component';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { MonitorComponent } from './components/monitor/monitor.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};




@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		ChannelsComponent,
		AdminComponent,
		LeaderboardComponent,
		MonitorComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		BrowserAnimationsModule,
		DragDropModule,
		HttpClientModule,
		SwiperModule
	],
	providers: [
		SocketService,
		GeneralService,
		{
			provide: SWIPER_CONFIG,
			useValue: DEFAULT_SWIPER_CONFIG
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
