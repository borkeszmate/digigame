import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';


@Injectable({
	providedIn: 'root'
})

export class GeneralService {
	// private url: string = 'http://localhost:3000';
	private url: string = 'https://digiminer.hps.digital';
	private userSource = new BehaviorSubject({user: undefined});
	currentUser = this.userSource.asObservable();

	private gameStartSource = new BehaviorSubject({isStarted: false});
	gameStart = this.gameStartSource.asObservable();

	constructor(private http: HttpClient) {
	}

	saveUser(user) {
		const url = this.url + '/api/save-user';
		return this.http.post(url, user);
	}

	addExistingUser(user) {
		const url = this.url + '/api/add-existing-user';
		return this.http.post(url, user);
	}

	restartGame() {
		const url = this.url + '/api/clear-users';
		return this.http.get(url);
	}

	transferUserData(user) {
		this.userSource.next(user);
	}

	gameStarted(isStarted) {
		console.log(isStarted)
		this.gameStartSource.next(isStarted);
	}

	getUsers() {
		const url = this.url + '/api/get-users'
		return this.http.get(url);
	}

	onGameFinish(user) {
		const url = this.url + '/api/game-finish'
		return this.http.post(url, user);
	}

	getResults() {
		const url = this.url + '/api/get-results'
		return this.http.get(url);
	}


}
