import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class GameplayService {

	investments = [
		{
			title: 'Intern',
			division: 'Digital',
			price: 10,
			DCKatt: 0,
			DPS: 0.5,
			isAvailable: function (usedInvestments, currentUser) {
				return (currentUser.points >= this.price) ? true : false;
			},
			imgUrl: '/assets/img/profile_pics/Gyakornok.png'
		},
		{
			title: 'Project Manager',
			division: 'Digital',
			price: 100,
			DCKatt: 1,
			DPS: 3,
			isAvailable: function (usedInvestments, currentUser) {
				return (currentUser.points >= this.price) ? true : false;
			},
			imgUrl: '/assets/img/profile_pics/PM.png'
		},
		{
			title: 'Copywriter',
			division: 'Creative',
			price: 500,
			DCKatt: 2,
			DPS: 18,
			isAvailable: function (usedInvestments, currentUser) {
				return (Object.keys(usedInvestments).includes('Project Manager') && currentUser.points >= this.price) ? true : false;
			return true
			},
			imgUrl: '/assets/img/profile_pics/Copywriter.png'
		},
		{
			title: 'Webdesigner',
			division: 'Creative',
			price: 1500,
			DCKatt: 10,
			DPS: 36,
			isAvailable: function (usedInvestments, currentUser) {
				return (Object.keys(usedInvestments).includes('Project Manager') && currentUser.points >= this.price) ? true : false;
			},
			imgUrl: '/assets/img/profile_pics/Webdesigner.png'
		},
		{
			title: 'Strategyst',
			division: 'Creative',
			// price: 0,
			price: 7500,
			DCKatt: 0,
			DPS: 250,
			isAvailable: function (usedInvestments, currentUser) {
				return (Object.keys(usedInvestments).includes('Project Manager') && currentUser.points >= this.price && usedInvestments['Strategyst'] === undefined) ? true : false;
			},
			imgUrl: '/assets/img/profile_pics/Stratega.png'
		},
		{
			title: 'Customer Service Team',
			division: 'Social',
			price: 15000,
			DCKatt: 99,
			DPS: 200,
			isAvailable: function (usedInvestments, currentUser) {
				return (Object.keys(usedInvestments).includes('Project Manager') && currentUser.points >= this.price) ? true : false;
			},
			imgUrl: '/assets/img/profile_pics/Customer_Service.png'
		},
		{
			title: 'Social Media Team',
			division: 'Social',
			price: 50000,
			DCKatt: 400,
			DPS: 400,
			isAvailable: function (usedInvestments, currentUser) {
				return (Object.keys(usedInvestments).includes('Copywriter') && currentUser.points >= this.price) ? true : false;
				// return true;
			},
			imgUrl: '/assets/img/profile_pics/SocialTeam.png'
		},
		{
			title: 'Performance Team',
			division: 'Performance',
			price: 50000,
			DCKatt: 125,
			DPS: 1250,
			isAvailable: function (usedInvestments, currentUser) {
				return (Object.keys(usedInvestments).includes('Copywriter') && currentUser.points >= this.price) ? true : false;
			},
			imgUrl: '/assets/img/profile_pics/PerformanceTeam.png'
		},

		{
			title: 'Developer Team',
			division: 'Digital',
			price: 200000,
			DCKatt: 666,
			DPS: 3000,
			isAvailable: function (usedInvestments, currentUser) {
				return (Object.keys(usedInvestments).includes('Webdesigner') && currentUser.points >= this.price) ? true : false;
			},
			imgUrl: '/assets/img/profile_pics/DevTeam.png'
		},
		{
			title: 'ONE MAN VR DEVELOPER',
			division: 'Digital',
			price: 1000000,
			DCKatt: 3500,
			DPS: 19999,
			isAvailable: function (usedInvestments, currentUser) {
				return (Object.keys(usedInvestments).includes('Developer Team') && currentUser.points >= this.price) ? true : false;
			},
			imgUrl: '/assets/img/profile_pics/VR_boy.png'
		},
	];




	constructor() { }

}
