import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { User } from '../models/user';


@Injectable({
	providedIn: 'root'
})

export class SocketService {
	private url: string = 'http://localhost:3000';
	// private url: string = 'https://digiminer.hps.digital';

	private socket;
	constructor() {
		this.socket = io(this.url);
	}

	public sendData(data) {
		this.socket.emit('new-data', data);
	}

	public updateUser(user) {
		this.socket.emit('update-user', user);
	}

	public startGame() {
		this.socket.emit('start-game');
	}

	public clearSingleUser(user: User) {
		this.socket.emit('clear-single-user', user);
	}

	public getData() {
		return Observable.create((observer) => {
			this.socket.on('new-data', (data) => {
				observer.next(data);
			});
		});
	}

	public getUsers() {
		return Observable.create((observer) => {
			this.socket.on('user', (data) => {
				observer.next(data);
			});
		});
	}

	public onGameStart() {
		return Observable.create((observer) => {
			this.socket.on('start-game', () => {
				observer.next();
			});
		});
	}

	public updateUsers() {
		return Observable.create((observer) => {
			this.socket.on('user-update', (user) => {
				observer.next(user);
			});
		});
	}

	public clearUsers() {
		return Observable.create((observer) => {
			this.socket.on('clear-users', (data) => {
				observer.next(data);
			});
		});
	}

	public onclearSingle() {
		return Observable.create((observer) => {
			this.socket.on('clear-single-user', (user) => {
				observer.next(user);
			});
		});
	}
}
